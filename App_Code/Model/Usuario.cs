﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Usuario
/// </summary>
public class Usuario
{
    
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string password;

        public string Password
        {
            get { return password;  }
            set { password = value; }
        }
        private int rol_id;

        public int Rol_id
        {
            get { return rol_id; }
            set { rol_id = value; }
        }

        public Usuario() { }

        public Usuario(int id, String nombre, String email, String password, int rol)
        {
            this.id = Id;
            this.nombre = Nombre;
            this.email = Email;
        this.password = Password;
            this.rol_id = Rol_id;
        }

    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Roles
/// </summary>
public class Roles
{

    private int id;

    public int Id
    {
        get { return id; }
        set { id = value; }
    }
    private string nombre;

    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }
    private string descripcion;

    public string Descripcion
    {
        get { return descripcion; }
        set { descripcion = value; }
    }

 

    public Roles() { }

    public Roles(int id, String nombre, String descripcion)
    {
        this.id = Id;
        this.nombre = Nombre;
        this.descripcion = Descripcion;
       
    }


}
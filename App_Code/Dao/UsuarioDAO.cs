﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for UsuarioDAO
/// </summary>
public class UsuarioDAO
{

    private static UsuarioDAO daoUsuario = null;
    private UsuarioDAO() { }

    #region
    public static UsuarioDAO getInstance()
    {
        if (daoUsuario == null)
        {
            daoUsuario = new UsuarioDAO();
        }
        return daoUsuario;
    }
    #endregion
    public Usuario LoginAcceso(String email, String pass)
    {

        SqlDataReader dr = null;
        Usuario objUsuario = null; //objeto empleado
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "loginUsuarios";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            con.Open();
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@password", pass);// parametros
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {

                objUsuario = new Usuario();
                objUsuario.Id = Convert.ToInt32(dr["id"].ToString());
                objUsuario.Nombre = dr["nombre"].ToString();
                objUsuario.Rol_id = Convert.ToInt32(dr["rol_id"].ToString());
                objUsuario.Email = dr["email"].ToString();
                objUsuario.Password = dr["password"].ToString();




            }
        }
        catch (Exception e)
        {
            objUsuario = null;
            throw e;
        }
        finally
        {

        }

        return objUsuario;
    }
}
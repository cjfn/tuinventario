﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;

/// <summary>
/// Summary description for RolesDAO
/// </summary>
public class RolesDAO
{
    public RolesDAO() { }

    public static string constr//conexion web.config
    {
        get { return ConfigurationManager.ConnectionStrings["conn"].ConnectionString; }
    }

    public static string Provider//proveedor de bases de datos
    {
        get { return ConfigurationManager.ConnectionStrings["conn"].ProviderName; }
    }

    public static DbProviderFactory dpf
    {
        get { return DbProviderFactories.GetFactory(Provider); }
    }

    
    public List<Roles> select_all_roles()
    {
        List<Roles> LstRol = new List<Roles>();

        string StoredProcedure = "spListarRoles";//nombre del procedimiento
        using (DbConnection con = dpf.CreateConnection())// creo  proveedor de conexion
        {
            con.ConnectionString = constr;// instancio la conexion
            using (DbCommand cmd = dpf.CreateCommand())//creo un comando sp
            {
                cmd.Connection = con; //conexion
                cmd.CommandText = StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure; //tipo de stored procedure
                con.Open(); //abro cone3xion
                using (DbDataReader dr = cmd.ExecuteReader())
                {
                    while(dr.Read())
                    {
                        Roles ObjRol = new Roles();
                        ObjRol.Id = Convert.ToInt32(dr["id"].ToString());
                        ObjRol.Nombre = (dr["nombre"].ToString());
                        ObjRol.Descripcion = (dr["descripcion"].ToString());

                        LstRol.Add(ObjRol);
                    }
                }
            }
        }
        return LstRol;
    }
    
}
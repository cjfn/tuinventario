﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LoginAcceso
/// </summary>
public class LoginAcceso
{
    #region
    private static LoginAcceso objUsuario = null;

    private LoginAcceso() { }

    public static LoginAcceso getInstance()
    {
        if (objUsuario == null)
        {
            objUsuario = new LoginAcceso();
        }
        return objUsuario;
    }

    #endregion

    public Usuario AccesoSistema(String email, String password)
    {
        try
        {
            return UsuarioDAO.getInstance().LoginAcceso(email, password);
        }
        catch (Exception e)
        {
            throw e;
        }
    }




}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class View_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["username"]!=null)
        {
            if (Int32.Parse(Session["rol"].ToString())==1)
            {
                Response.Redirect("Modulos/Admin/HomeAdmin.aspx");
                Response.Write("<script>alert('usuario WEBMASTER');</script>");
            }
            if (Int32.Parse(Session["rol"].ToString()) == 2)
            {
                Response.Redirect("Modulos/Admin/HomeAdmin.aspx");
                Response.Write("<script>alert('usuario ADMIN');</script>");
            }
            if (Int32.Parse(Session["rol"].ToString()) == 3)
            {
                Response.Redirect("Modulos/Worker/HomeWorker.aspx");
                Response.Write("<script>alert('usuario EMPLEADO');</script>");
            }
        }
        else
        {

        }
    }

    protected void btnIngresar_Click(object sender, EventArgs e)
    {
        Usuario ObjUsuario = LoginAcceso.getInstance().AccesoSistema(txtEmail.Text, txtPassword.Text);

        if (ObjUsuario != null)
        {

            
            Response.Write("<script>alert('usuario correcto');</script>");
            Session["username"] = ObjUsuario.Nombre;
            Session["rol"] = ObjUsuario.Rol_id;
            Session["email"] = ObjUsuario.Email;

            if (ObjUsuario.Rol_id == 1)
            {
                Response.Redirect("Modulos/Admin/HomeAdmin.aspx");
                Response.Write("<script>alert('usuario WEBMASTER');</script>");
            }
            if (ObjUsuario.Rol_id == 2)
            {
                Response.Redirect("Modulos/Admin/HomeAdmin.aspx");
                Response.Write("<script>alert('usuario ADMIN');</script>");
            }
            if (ObjUsuario.Rol_id == 3)
            {
                Response.Redirect("Modulos/Worker/HomeWorker.aspx");
                Response.Write("<script>alert('usuario EMPLEADO');</script>");
            }
        }
        else
        {
            Response.Write("<script>alert('usuario incorrecto');</script>");
        }


    }
}
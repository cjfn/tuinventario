﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/View/Home.master" CodeFile="AdminUsuarios.aspx.cs" Inherits="View_Modulos_Admin_AdminUsuarios" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />

   <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="id"
 OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting"  CssClass="table table-striped table-bordered table-hover" EmptyDataText="No records has been added." ssClass="table table-striped table-bordered table-hover">
<Columns>
      <asp:TemplateField HeaderText="Id" ItemStyle-Width="150" visible="false">
        <ItemTemplate>
            <asp:Label ID="lblId" runat="server" Text='<%# Eval("id") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtId" runat="server" Text='<%# Eval("id") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Nombre" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblNombre" runat="server" Text='<%# Eval("nombre") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtNombre" runat="server" Text='<%# Eval("nombre") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("email") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtEmail" runat="server" Text='<%# Eval("email") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
     <asp:TemplateField HeaderText="Password" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblPassword" runat="server" Text='<%# Eval("password") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtPassword" runat="server" Text='<%# Eval("password") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
     <asp:TemplateField HeaderText="Rol" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblRol" runat="server" Text='<%# Eval("rol_id") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:DropDownList ID="ddlRol" runat="server" DataSourceID="conn" DataTextField="nombre" DataValueField="id" >
        </asp:DropDownList>
           
        </EditItemTemplate>
    </asp:TemplateField>
    <asp:CommandField ButtonType="Button" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>
</Columns>
</asp:GridView>
<table class="table table-bordered table-responsive" border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
<tr>
    <td style="width: 150px">
        nombre:<br />
        <asp:TextBox ID="txtNombre" runat="server" Width="140" />
    </td>
    <td style="width: 150px">
        email:<br />
        <asp:TextBox ID="txtEmail" runat="server" Width="140" />
    </td>
    <td style="width: 150px">
        password:<br />
        <asp:TextBox ID="txtPassword" runat="server" Width="140" />
    </td>
    <td style="width: 150px">
        rol:<asp:DropDownList ID="ddlRol" runat="server" DataSourceID="conn" DataTextField="nombre" DataValueField="id">
        </asp:DropDownList>
        <asp:SqlDataSource ID="conn" runat="server" ConnectionString="<%$ ConnectionStrings:MiPuntoDeVentaConnectionString %>" SelectCommand="spListarRoles" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <br />
    </td>
    <td style="width: 100px">
        <asp:Button ID="btnAdd" runat="server" Text="Agregar" OnClick="Insert" />
    </td>
</tr>
</table>
</asp:Content>

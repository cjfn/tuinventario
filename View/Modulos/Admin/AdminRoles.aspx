﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/View/Home.master" CodeFile="AdminRoles.aspx.cs" Inherits="View_Modulos_Admin_AdminRoles" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
        <h1 align="center">BIENVENIDO A SU PUNTO DE VENTA</h1>

     <div class="table-responsive">

                  <table class="table table-hover table-bordered" id="MyTable">
                    <thead>
                      <tr>
                        <th>
                          Id
                        </th>
                        <th>
                          Nombre
                        </th>
                        <th>
                          Descripcion 
                        </th>
                  </thead>
                </table>
                </div>          
       <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
       <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>
       <script type="text/javascript">

           $(document).ready(function () {
               $.ajax({
                   url: '/View/WebService/RolesWebService.asmx/GetRoles',
                   method: 'post',
                   dataType: 'json',
                   success: function (data) {
                       $('#MyTable').dataTable({
                           data: data,
                           columns: [
                              { 'data': 'Id' },
                              { 'data': 'Nombre' },
                              { 'data': 'Descripcion' },
                            
                           ],
                            buttons: [
                               'copy', 'excel', 'pdf'
                           ]
                       });
                   }
               });
           });
       </script>
             
   
</asp:Content>



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class View_Modulos_Worker_HomeWorker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    //metodo para retornar el asmx o webservice rest json
    [WebMethod]
    public static List<Roles> ListarRoles()
    {
        List<Roles> Lista = null;
        try
        {
            Lista = ListarRoles();
        }
        catch(Exception ex)
        {
            Lista = null;
        }
        return Lista;
    }
}
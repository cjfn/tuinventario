﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class View_Modulos_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("~/View/Login.aspx");
        }
        else
        {
            lblUser.Text = Session["username"].ToString();

            if (!Page.IsPostBack)
            {
               
            }
        }
    }

    protected void Logout_Click(object sender, EventArgs e)
    {
        Response.Write("<script>alert('Cerrando sesion');</script>");
        Session["username"] = "";
        Session["rol"] = "";
        Session["email"] = "";
        Response.Redirect("~/View/Login.aspx");
    }
}

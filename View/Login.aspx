﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="View_Login" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Acceso</title>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link href="css/AdminLTE.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">
			<div class="search-box">
				<div class="caption">
					<h3>Login</h3>
					<p>Ingreso de usuarios</p>
				</div>
				<form id="form1" runat="server" action="" class="loginForm">
					<div class="input-group">
						
                        <asp:TextBox ID="txtEmail"  runat="server" CssClass="form-control" placeholder="ingrese usuario..." require="true"></asp:TextBox>
						<asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" require="true" placeholder="ingrese contraseña..." TextMode="Password"></asp:TextBox>
                        <asp:Button ID="btnIngresar" runat="server" Text="Iniciar Sesión"  CssClass="btn btn-success" OnClick="btnIngresar_Click"  value="Submit" />
						
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-4">
			<div class="aro-pswd_info">
				<div id="pswd_info">
					
				</div>
			</div>
		</div>
	</div>
</div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
</body>
</html>